public class Carrera {
  public static int x = 0;

  private static class Inc extends Thread {
    public void run() {
      for (int i = 0; i < 1000; i++)
        x = x + 1;
    }
  }

  private static class Dec extends Thread {
    public void run() {
      for (int i = 0; i < 1000; i++)
        x = x - 1;
    }
  }

  public static void main(String[] args) throws Exception {
    Inc inc = new Inc();
    Dec dec = new Dec();
    inc.start();
    dec.start();
    inc.join();
    dec.join();
    System.out.println(x);
  }
}
