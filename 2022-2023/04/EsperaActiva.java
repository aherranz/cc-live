public class EsperaActiva {
  static final int N_OPS = 1000;
  static volatile int x = 0;
  static volatile boolean enSC = false;

  static class Inc extends Thread {
    public void run() {
      for (int i = 0; i < N_OPS; i++) {
        while (enSC) {}
        enSC = true;
        x = x + 1;
        enSC = false;
      }
    }
  }

  static class Dec extends Thread {
    public void run() {
      for (int i = 0; i < N_OPS; i++) {
        while (enSC) {}
        enSC = true;
        x = x - 1;
        enSC = false;
      }
    }
  }

  public static void main(String[] args)
    throws InterruptedException {
    Thread i = new Inc();
    Thread d = new Dec();
    i.start(); d.start();
    i.join(); d.join();
    System.out.println(x);
  }
}
