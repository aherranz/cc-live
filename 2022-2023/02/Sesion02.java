public class Sesion02 {
  public static class A extends Thread {
    int x;

    public void run() {
      System.out.println("Hola");
    }
  }

  public static void main(String args[]) throws Exception {
    A a1 = new A();
    A a2 = new A();
    a1.start();
    a2.start();
    a1.join();
    a1.start();
  }
}
