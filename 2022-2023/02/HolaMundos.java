public class HolaMundos {
  private static void sleep(int ms) {
    try { Thread.sleep(ms); } catch (Exception e) {}
  }
  public static class Tierra extends Thread {
    public void run() {
      while (true) {
        System.out.println("Una Tierra: empieza orbita");
        HolaMundos.sleep(10 * 365);
        System.out.println("Una Tierra: termina órbita");
      }
    }
  }

  public static class Trisolaris extends Thread {
    public void run() {
      while (true) {
        System.out.println("Una Trisolaris: empieza orbita");
        HolaMundos.sleep(10 * 100);
        System.out.println("Una Trisolaris: termina órbita");
      }
    }
  }

  public static void main(String args[]) throws InterruptedException {
    Tierra tierra = new Tierra();
    Trisolaris trisolaris = new Trisolaris();

    tierra.start();
    trisolaris.start();

    tierra.join();
    trisolaris.join();

    System.out.println("Soy el proceso 'main()'");
  }
}
