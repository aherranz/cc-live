public class IdentificaProcesos {
  private static final int N = 10;

  public static void dormir(int ms) {
    try { Thread.sleep(ms); } catch (Exception e) { }
  }

  public static class ProcesoIdentificado extends Thread {
    private int id;
    public ProcesoIdentificado(int id) {
      this.id = id;
    }
    public void run() {
      System.out.printf("INICIO proceso %d\n", id);
      dormir(1000 * id);
      System.out.printf("FIN proceso %d\n", id);
    }
  }

  public static void main(String args[]) throws InterruptedException {
    Thread[] procesosIdentificados = new Thread[N];

    for (int i = 0; i < N; i++)
      procesosIdentificados[i] = new ProcesoIdentificado(i);

    for (int i = 0; i < N; i++)
      procesosIdentificados[i].start();

    for (int i = 0; i < N; i++)
      procesosIdentificados[i].join();

    System.out.printf("MAIN: todos los procesos han terminado\n");
  }
}
