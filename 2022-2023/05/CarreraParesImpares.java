import java.util.Queue;

public class CarreraParesImpares {
  private static final int N = 1000;

  public static class ProductorPares extends Thread {
    private Queue<Integer> cola;
    public ProductorPares(Queue<Integer> cola) {
      this.cola = cola;
    }
    public void run() {
      for (int i = 0; i < N; i++) {
        cola.add(i*2);
      }
    }
  }

  public static class ProductorImpares extends Thread {
    private Queue<Integer> cola;
    public ProductorImpares(Queue<Integer> cola) {
      this.cola = cola;
    }
    public void run() {
      for (int i = 0; i < N; i++) {
        cola.add(i*2+1);
      }
    }
  }

  public static class ConsumidorParesImpares extends Thread {
    private Queue<Integer> cola;
    public ConsumidorParesImpares(Queue<Integer> cola) {
      this.cola = cola;
    }
    public void run() {
      for (int i = 0; i < 2*N; i++) {
        // Espera activa para *intentar* no consumir de cola vacía
        while(cola.isEmpty()) {
          // No hacer nada
        }

        System.out.println(cola.poll());
      }
    }
  }

  public static void main(String args[]) throws InterruptedException {
    // Cola compartida
    Queue<Integer> q =
      new java.util.ArrayDeque<Integer>();
      // new java.util.LinkedList<Integer>();

    Thread p, i, c;
    p = new ProductorPares(q);
    i = new ProductorImpares(q);
    c = new ConsumidorParesImpares(q);

    p.start();
    i.start();
    c.start();

    p.join();
    i.join();
    c.join();
  }
}
