class Dato {
  public volatile int x = 0;
}
  
public class Carrera2 {
  public static volatile int x = 0;

  public static class Inc extends Thread {
    Dato dato;

    Inc(Dato d) {
      dato = d;
    }

    public void run() {
      for (int i = 0; i < 10000; i++)
        dato.x = dato.x + 1;
    }
  }

  public static class Dec extends Thread {
    Dato dato;

    Dec(Dato d) {
      dato = d;
    }

    public void run() {
      for (int i = 0; i < 10000; i++)
        dato.x = dato.x - 1;
    }
  }

  public static void main(String[] a) throws Exception {
    Dato dat = new Dato();
    Inc i = new Inc(dat);
    Dec d = new Dec(dat);
    i.start();
    d.start();
    i.join();
    d.join();
    System.out.println(dat.x);
  }
}
