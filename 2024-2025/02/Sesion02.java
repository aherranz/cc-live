class Global {
  static int dato;
}

class HolaMundo1 extends Thread {
  public HolaMundo1() {
  }
  public void run() {
    System.out.println("Hola mundo 1");
    Global.dato = 42;
  }
}

class HolaMundo2 extends Thread {
  public HolaMundo2() {
  }
  public void run() {
    System.out.println("Hola mundo 2");
    System.out.println("Dato: " + Global.dato);
  }
}

public class Sesion02 {
  public static void main(String[] args) {
    Thread hola1 = new HolaMundo1();
    Thread hola2 = new HolaMundo2();
    hola1.start();
    hola2.start();
    System.out.println("Programa terminado");
  }
}
