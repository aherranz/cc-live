public class Carrera {
  public static volatile int x = 0;
  public static volatile boolean enSC = false;

  public static class Inc extends Thread {
    public void run() {
      for (int i = 0; i < 1000; i++) {
        while(enSC);
        enSC = true;
        x = x + 1;
        enSC = false;
      }
    }
  }

  public static class Dec extends Thread {
    public void run() {
      for (int i = 0; i < 1000; i++)
        while(enSC);
        enSC = true;
        x = x - 1;
        enSC = false;
    }
  }

  public static void main(String[] a) throws Exception {
    Inc i = new Inc();
    Dec d = new Dec();
    i.start();
    d.start();
    i.join();
    d.join();
    System.out.println(x);
  }
}
